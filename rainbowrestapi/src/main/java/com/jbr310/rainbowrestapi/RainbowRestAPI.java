package com.jbr310.rainbowrestapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class RainbowRestAPI {
    @CrossOrigin
    @GetMapping("/rainbow")
    public ArrayList<String> getRainbow() {
        ArrayList<String> rainbow = new ArrayList<String>();
        rainbow.add("red");
        rainbow.add("orange");
        rainbow.add("yellow");
        rainbow.add("green");
        rainbow.add("blue");
        rainbow.add("indigo");
        rainbow.add("violet");
        return rainbow;
    }
}
